<title>個人情報更新</title>
</head>
<body>
	<h1>
		個人情報更新 <span class="loginInfo">ログイン者名：${ sessionScope.LOGIN_EMP.nmEmployee }</span>
	</h1>
	<form action="privateCheck" method="post">
		<c:if test="${errMsg != null }">
			<div class="msgBox error">
				<c:forEach items="${errMsg }" var="errList">
					<c:out value="${errList }" />
				</c:forEach>
			</div>
		</c:if>
		<table>
			<tr>
				<th class="header">社員名</th>
				<td><input type="text" name="empNm" placeholder="社員名"
					value="${ sessionScope.userData.getNmEmployee() }" required="required" maxlength="50" /></td>
			</tr>
			<tr>
				<th class="header">社員名フリガナ</th>
				<td><input type="text" name="empKn" placeholder="フリガナ"
					value="${ sessionScope.userData.getKnEmployee() }" required="required" maxlength="50" /></td>
			</tr>
			<tr>
				<th class="header">メールアドレス</th>
				<td><input type="text" name="mail" placeholder="メールアドレス"
					value="${ sessionScope.userData.getMailAddress() }" required="required" maxlength="100" /></td>
			</tr>
			<tr>
				<th class="header">パスワード</th>
				<td><input type="password" name="pass" placeholder="※更新する場合は入力" />
					<input type="hidden" name="passOld" value="${ passOld }"
					pattern="^(?=.*?\d)[a-zA-Z\d]{3,10}$" /></td>
			</tr>
			<tr>
				<th class="header">パスワード確認</th>
				<td><input type="password" name="passConf"
					placeholder="※更新する場合は再入力" /></td>
			</tr>
			<tr>
				<td colspan="2" class="btnArea">
				<input type="submit" name="submit" value="更新" />
				<input type="submit" name="restore" value="再取得"/>
				</td>
			</tr>
		</table>
	</form>

</body>
</html>