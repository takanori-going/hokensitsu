package com.rhizome.example.page;

import java.util.List;
import java.util.Map;

import javax.servlet.annotation.WebServlet;

import com.rhizome.example.entity.Employee;
import com.rhizome.example.entity.EmployeeInfo;
import com.rhizome.example.page.base.BaseServlet;
import com.rhizome.example.service.SearchService;

@WebServlet(name = "login", urlPatterns = { "/login" })
public class LoginAction extends BaseServlet {

	@Override
	protected String getPageName() {
		return "login";
	}

	@Override
	protected String doAction() throws Exception {
		// 7画面入力項目の取得
		//’ここにメールアドレスとパスワードが格納される
		String[] param = super.getInputParameter("mail", "pass");

		// 入力チェック
		/*
		 * 必須チェックはHTML側で済みのため、Java側は特になし
		 */

		// ’ログイン処理
		SearchService service = new SearchService();
		Map<String, Object> loginData = service.doLogin(param[0], param[1]);

		// ’結果の取得（検索結果0件）
		if (loginData == null || loginData.get("EMP") == null) {
			//’後でここを編集
			throw new Exception("IDまたはパスワードが間違っています");
		}

		//’Employeeをインスタンス化し、、メールアドレスをセット
		Employee emm = new Employee();
		emm.setMailAddress(param[0]);

		//’メールアドレスから社員情報を検索しリストにEmployeeInfo型で格納
		List<EmployeeInfo> mInfo = service.searchEmployeeInfo(emm);

		//emmに検索結果を上書き
		emm = mInfo.get(0).getEmployee();



		//’セッションにこのユーザーのメールアドレスを格納、プライベートアップデートの入力値との比較に使用
		//’セッションに取得した社員情報をEmployeeインスタンスで送信
		super.session.setAttribute("userData", emm);


		//’ 取得した情報をセッションへ格納
		super.session.setAttribute("LOGIN_EMP", loginData.get("EMP"));
		super.session.setAttribute("DEP_LIST", loginData.get("DEP_LIST"));


		//’管理者権限を持つかどうか判定する：行える社員、一番の山田
		if ("yamada@hoge.jp".equals(param[0])) {
			//’管理者はそのままメニューに飛びすべての操作を実行可能
			return "menu";

			//’管理者権限を持たないときは、社員情報を送信したうえでprivateUpdateに飛ばす
		} else {

			//’名前、読み、メールアドレスをprivateUpdateで初期表示させるため送信
			super.request.setAttribute("empNm", emm.getNmEmployee());
			super.request.setAttribute("empKn", emm.getKnEmployee());
			super.request.setAttribute("mail", emm.getMailAddress());

			return "privateUpdate";
		}
	}

	/*’管理者権限を持たない社員の時
	 * メールアドレスから社員情報を検索し戻り値をセッションに格納、
	protected void doSrch(String mail) {
		SearchService
	}*/
}
