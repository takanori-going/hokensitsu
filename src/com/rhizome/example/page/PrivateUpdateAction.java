/*
 * InputCheckAction.java
 *   作成	IT-College	2016
 *------------------------------------------------------------
 * Copyright(c) Rhizome Inc. All Rights Reserved.
 */
package com.rhizome.example.page;

import javax.servlet.annotation.WebServlet;

import com.rhizome.example.entity.Employee;
import com.rhizome.example.page.base.BaseServlet;
import com.rhizome.example.service.SearchService;
import com.rhizome.example.service.UpdateService;
import com.rhizome.example.service.UpdateService.UPDATE_MODE;

/**
 * 一般社員が情報の更新を行う際に使用するクラス
 * メールアドレスが既に登録されているか判定します（自分のメールアドレス以外で）
 * @author IT-College
 *
 */
@WebServlet(name = "privateCheck", urlPatterns = { "/privateCheck" })
public class PrivateUpdateAction extends BaseServlet {

	@Override
	protected String getPageName() {
		return "privateResult";
	}

	@Override
	protected String doAction() throws Exception {
		SearchService service = new SearchService();

		//LoginActionから送信されたsessionを受け取る、ここにはログインした人の情報が格納される
		Employee userIn = (Employee) super.session.getAttribute("userData");

		//’押されたボタンを判定、
		if (super.request.getParameter("restore") != null) {
			//’再取得が押下された場合
			Employee restore = service.searchEmployeeByPkey(userIn.getIdEmployee());

			if(restore==null || restore.getIdDepartment()==null) {
				//’管理者がログイン中に自分の登録情報を削除してしまった時の対応
				super.message = "ログイン情報がありません、ログイン画面に戻ります";

				return "login";
			}
			//session領域のユーザー情報を更新
			super.session.setAttribute("userData", restore);

			//’もう一度同じ画面に遷移
			return "privateUpdate";
		}
		String[] pageParam = super.getInputParameter(
				"empId" //0
				, "empNm" // 1
				, "empKn" // 2
				, "mail" // 3
				, "pass" // 4
				, "passConf" //5確認用のパスワード、ひとしくなければエラーを飛ばす
				, "depId" //6
		);

		//’もしメールアドレスに変更を加えなかったらなにもしない
		if (userIn.getMailAddress().equals(pageParam[3])) {

			//’打ち込まれたメールアドレスが登録済みだったら、エラーを飛ばす
		} else if (service.checkDuplicationMail(null, pageParam[3])) {
			throw new Exception("入力されたメールアドレスは既に存在しています");
		}

		//’パスワードに変更を加えなかったとき
		if (pageParam[4].equals("") && pageParam[5].equals("")) {
			pageParam[4] = userIn.getPassword();
		} else if (pageParam[4] == null) {
			throw new Exception("パスワードを変更する際は二回入力してください「");
		} else if (pageParam[5] == null) {
			throw new Exception("確認用の場所にも入力してください");
		}
		//’入力されたパスワードが一致しないとき
		else if (!pageParam[4].equals(pageParam[5])) {
			//’例外を送信
			throw new Exception("パスワードが一致しません");
		}

		//’上の条件をすべてクリアしたらEmployeeインスタンスのうち変更をセットする
		userIn.setNmEmployee(pageParam[1]);
		userIn.setKnEmployee(pageParam[2]);
		userIn.setMailAddress(pageParam[3]);
		userIn.setPassword(pageParam[4]);

		//session領域のユーザー情報を更新
		super.session.setAttribute("userData", userIn);

		//’更新の実行
		UpdateService usrv = new UpdateService();
		usrv.registEmployee(userIn, UPDATE_MODE.UPDATE);

		return "privateResult";
	}
}
